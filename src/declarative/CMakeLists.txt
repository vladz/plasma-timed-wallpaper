add_definitions(-DTRANSLATION_DOMAIN=\"plasma_wallpaper_org.kde.timed\")

set(FALLBACK_WALLPAPER "Timed Numbers")
configure_file(config-timedwallpaper.h.cmake ${CMAKE_CURRENT_BINARY_DIR}/config-timedwallpaper.h)

set(timedwallpaperplugin_SOURCES
    timedwallpapercrawler.cpp
    timedwallpaperdescription.cpp
    timedwallpaperengine.cpp
    timedwallpaperengine_solar.cpp
    timedwallpaperengine_timed.cpp
    timedwallpaperextensionplugin.cpp
    timedwallpaperhandler.cpp
    timedwallpaperimagehandle.cpp
    timedwallpaperimageprovider.cpp
    timedwallpapermodel.cpp
    timedwallpaperpreviewprovider.cpp
)

add_library(plasma_wallpaper_timedplugin ${timedwallpaperplugin_SOURCES})

target_link_libraries(plasma_wallpaper_timedplugin
    Qt5::Core
    Qt5::Gui
    Qt5::Positioning
    Qt5::Qml

    KF5::ConfigCore
    KF5::I18n
    KF5::KIOWidgets
    KF5::Package

    KTimedWallpaper::KTimedWallpaper
)

install(TARGETS plasma_wallpaper_timedplugin DESTINATION ${KDE_INSTALL_QMLDIR}/org/kde/plasma/wallpapers/timed)
install(FILES qmldir DESTINATION ${KDE_INSTALL_QMLDIR}/org/kde/plasma/wallpapers/timed)
