/*
 * SPDX-FileCopyrightText: 2020 Vlad Zahorodnii <vlad.zahorodnii@kde.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "timedwallpapercrawler.h"

#include <KTimedWallpaperMetaData>
#include <KTimedWallpaperReader>

#include <QDir>

/**
 * \class TimedWallpaperCrawler
 * \brief The TimedWallpaperCrawler class discovers timed wallpapers.
 */

/**
 * Constructs a TimedWallpaperCrawler object with the given \p parent.
 */
TimedWallpaperCrawler::TimedWallpaperCrawler(QObject *parent)
    : QThread(parent)
    , m_token(QUuid::createUuid())
{
}

/**
 * Destructs the TimedWallpaperCrawler object.
 */
TimedWallpaperCrawler::~TimedWallpaperCrawler()
{
    wait();
}

/**
 * Returns the UUID that uniquely identifies this crawler.
 */
QUuid TimedWallpaperCrawler::token() const
{
    return m_token;
}

/**
 * Sets the list of directories where timed wallpapers should be searched to \p roots.
 *
 * Timed wallpapers will be searched recursively.
 */
void TimedWallpaperCrawler::setSearchRoots(const QStringList &roots)
{
    m_searchRoots = roots;
}

/**
 * Returns the list of directories where timed wallpapers should be searched recursively.
 */
QStringList TimedWallpaperCrawler::searchRoots() const
{
    return m_searchRoots;
}

/**
 * Sets the package structure for timed wallpaper packages to \p structure.
 */
void TimedWallpaperCrawler::setPackageStructure(KPackage::PackageStructure *structure)
{
    m_packageStructure = structure;
}

/**
 * Returns the package structure for timed wallpaper packages.
 */
KPackage::PackageStructure *TimedWallpaperCrawler::packageStructure() const
{
    return m_packageStructure;
}

void TimedWallpaperCrawler::run()
{
    for (const QString &candidate : qAsConst(m_searchRoots))
        visitFolder(candidate);

    deleteLater();
}

void TimedWallpaperCrawler::visitFolder(const QString &filePath)
{
    QDir currentFolder(filePath);
    currentFolder.setFilter(QDir::NoDotAndDotDot | QDir::NoSymLinks | QDir::Readable | QDir::AllDirs | QDir::Files);
    currentFolder.setNameFilters({ QStringLiteral("*.heic"), QStringLiteral("*.heif") });

    const QFileInfoList fileInfos = currentFolder.entryInfoList();
    for (const QFileInfo &fileInfo : fileInfos) {
        if (fileInfo.isDir()) {
            if (checkPackage(fileInfo.filePath())) {
                emit foundPackage(fileInfo.filePath(), token());
            } else {
                visitFolder(fileInfo.filePath());
            }
        } else {
            visitFile(fileInfo.filePath());
        }
    }
}

void TimedWallpaperCrawler::visitFile(const QString &filePath)
{
    // Not every heif file is a timed wallpaper, we need to read the file contents to
    // determine whether filePath actually points to a timed wallpaper file.
    KTimedWallpaperReader reader(filePath);

    // If the first image has valid metadata, assume that it's indeed a timed wallpaper.
    KTimedWallpaperMetaData metaData = reader.metaDataAt(0);
    if (metaData.isValid())
        emit foundFile(filePath, token());
}

bool TimedWallpaperCrawler::checkPackage(const QString &filePath) const
{
    if (!QFile::exists(filePath + QLatin1String("/metadata.desktop")) &&
            !QFile::exists(filePath + QLatin1String("/metadata.json")))
        return false;

    KPackage::Package package(packageStructure());
    package.setPath(filePath);

    const QUrl imageUrl = package.fileUrl(QByteArrayLiteral("timed"));
    return imageUrl.isValid();
}
