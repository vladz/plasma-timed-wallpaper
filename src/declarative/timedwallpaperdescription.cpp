/*
 * SPDX-FileCopyrightText: 2020 Vlad Zahorodnii <vlad.zahorodnii@kde.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "timedwallpaperdescription.h"
#include "timedwallpaperimagehandle.h"

#include <KTimedWallpaperReader>

/**
 * Constructs an invalid TimedWallpaperDescription object.
 */
TimedWallpaperDescription::TimedWallpaperDescription()
{
}

/**
 * Returns \c true if the TimedWallpaperDescription is valid; otherwise returns \c false.
 */
bool TimedWallpaperDescription::isValid() const
{
    return !m_imageUrls.isEmpty();
}

/**
 * Returns a bitmask that indicates which wallpaper engines can display this wallpaper.
 */
TimedWallpaperDescription::EngineTypes TimedWallpaperDescription::supportedEngines() const
{
    EngineTypes types = SolarEngine | TimedEngine;
    for (const KTimedWallpaperMetaData &metaData : m_metaData) {
        // Exclude the solar engine if there's at least one image without solar metadata.
        if (!(metaData.fields() & KTimedWallpaperMetaData::SolarAzimuthField))
            types &= ~SolarEngine;
    }
    return types;
}

/**
 * Returns the total number of images in the timed wallpaper.
 *
 * This method will return \c 0 if the TimedWallpaperDescription object is invalid.
 */
int TimedWallpaperDescription::imageCount() const
{
    return m_imageUrls.count();
}

/**
 * Returns the url for the image with the specified index \p imageIndex.
 *
 * This method will return an invalid QImage if the TimedWallpaperDescription is invalid
 * or if the provided index is outside the valid range.
 */
QUrl TimedWallpaperDescription::imageUrlAt(int imageIndex) const
{
    return m_imageUrls.value(imageIndex);
}

/**
 * Returns the metadata for the image with the specified index \p imageIndex.
 *
 * This method will return an invalid KTimedWallpaperMetaData if the TimedWallpaperDescription
 * is invalid or if the provided index is outside the valid range.
 */
KTimedWallpaperMetaData TimedWallpaperDescription::metaDataAt(int imageIndex) const
{
    return m_metaData.value(imageIndex);
}

/**
 * Attempts to load the TimedWallpaperDescription for the given file name \p fileName.
 *
 * Returns a valid TimedWallpaperDescription if the loading succeeds; otherwise, the returned
 * description will be invalid.
 */
TimedWallpaperDescription TimedWallpaperDescription::fromFile(const QString &fileName)
{
    KTimedWallpaperReader reader(fileName);
    if (!reader.imageCount())
        return TimedWallpaperDescription();

    TimedWallpaperDescription description;

    for (int i = 0; i < reader.imageCount(); ++i) {
        const KTimedWallpaperMetaData metaData = reader.metaDataAt(i);
        if (!metaData.isValid())
            return TimedWallpaperDescription();

        TimedWallpaperImageHandle handle;
        handle.setFileName(fileName);
        handle.setImageIndex(i);

        description.addImage(handle.toUrl(), metaData);
    }

    return description;
}

/**
 * \internal
 */
void TimedWallpaperDescription::addImage(const QUrl &url, const KTimedWallpaperMetaData &metaData)
{
    m_imageUrls << url;
    m_metaData << metaData;
}
