/*
 * SPDX-FileCopyrightText: 2020 Vlad Zahorodnii <vlad.zahorodnii@kde.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <KTimedWallpaperMetaData>

#include <QString>
#include <QUrl>

class TimedWallpaperDescription
{
public:
    enum EngineType {
        SolarEngine = 1 << 0,
        TimedEngine = 1 << 1,
    };
    Q_DECLARE_FLAGS(EngineTypes, EngineType)

    TimedWallpaperDescription();

    bool isValid() const;
    EngineTypes supportedEngines() const;

    int imageCount() const;

    QUrl imageUrlAt(int imageIndex) const;
    KTimedWallpaperMetaData metaDataAt(int imageIndex) const;

    static TimedWallpaperDescription fromFile(const QString &fileName);

private:
    void addImage(const QUrl &url, const KTimedWallpaperMetaData &metaData);

    QVector<KTimedWallpaperMetaData> m_metaData;
    QVector<QUrl> m_imageUrls;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(TimedWallpaperDescription::EngineTypes)
