/*
 * SPDX-FileCopyrightText: 2020 Vlad Zahorodnii <vlad.zahorodnii@kde.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "timedwallpaperdescription.h"

#include <QDateTime>
#include <QMap>

class TimedWallpaperEngine
{
public:
    virtual ~TimedWallpaperEngine();

    void setDescription(const TimedWallpaperDescription &description);
    TimedWallpaperDescription description() const;

    void update();

    QUrl bottomLayer() const;
    QUrl topLayer() const;
    qreal blendFactor() const;

    virtual bool isExpired() const;

protected:
    virtual qreal progressForMetaData(const KTimedWallpaperMetaData &metaData) const = 0;
    virtual qreal progressForDateTime(const QDateTime &dateTime) const = 0;

private:
    TimedWallpaperDescription m_description;
    QMap<qreal, int> m_progressToImageIndex;
    QUrl m_topLayer;
    QUrl m_bottomLayer;
    qreal m_blendFactor;
};
