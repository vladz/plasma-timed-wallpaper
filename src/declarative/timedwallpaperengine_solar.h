/*
 * SPDX-FileCopyrightText: 2020 Vlad Zahorodnii <vlad.zahorodnii@kde.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "timedwallpaperengine.h"

#include <KSunPath>
#include <KSunPosition>

class TimedWallpaperEngineSolar : public TimedWallpaperEngine
{
public:
    bool isExpired() const override;

    static TimedWallpaperEngineSolar *create(const QGeoCoordinate &location);

protected:
    qreal progressForMetaData(const KTimedWallpaperMetaData &metaData) const override;
    qreal progressForDateTime(const QDateTime &dateTime) const override;

private:
    TimedWallpaperEngineSolar(const KSunPath &sunPath, const KSunPosition &midnight,
                              const QGeoCoordinate &location, const QDateTime &dateTime);
    qreal progressForPosition(const KSunPosition &position) const;

    KSunPath m_sunPath;
    KSunPosition m_midnight;
    QGeoCoordinate m_location;
    QDateTime m_dateTime;
};
