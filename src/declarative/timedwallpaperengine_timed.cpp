/*
 * SPDX-FileCopyrightText: 2020 Vlad Zahorodnii <vlad.zahorodnii@kde.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "timedwallpaperengine_timed.h"

TimedWallpaperEngineTimed *TimedWallpaperEngineTimed::create()
{
    return new TimedWallpaperEngineTimed();
}

qreal TimedWallpaperEngineTimed::progressForMetaData(const KTimedWallpaperMetaData &metaData) const
{
    return metaData.time();
}

qreal TimedWallpaperEngineTimed::progressForDateTime(const QDateTime &dateTime) const
{
    QDateTime midnight = dateTime;
    midnight.setTime(QTime());

    return midnight.secsTo(dateTime) / 86400.0;
}
