/*
 * SPDX-FileCopyrightText: 2020 Vlad Zahorodnii <vlad.zahorodnii@kde.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "timedwallpaperengine.h"

class TimedWallpaperEngineTimed : public TimedWallpaperEngine
{
public:
    static TimedWallpaperEngineTimed *create();

protected:
    qreal progressForMetaData(const KTimedWallpaperMetaData &metaData) const override;
    qreal progressForDateTime(const QDateTime &dateTime) const override;
};
