/*
 * SPDX-FileCopyrightText: 2020 Vlad Zahorodnii <vlad.zahorodnii@kde.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "timedwallpaperextensionplugin.h"
#include "timedwallpaperhandler.h"
#include "timedwallpaperimageprovider.h"
#include "timedwallpapermodel.h"
#include "timedwallpaperpreviewprovider.h"

#include <KSystemClockMonitor>

#include <QQmlEngine>

void TimedWallpaperExtensionPlugin::registerTypes(const char *uri)
{
    qmlRegisterType<TimedWallpaperHandler>(uri, 1, 0, "TimedWallpaperHandler");
    qmlRegisterType<TimedWallpaperModel>(uri, 1, 0, "TimedWallpaperModel");
    qmlRegisterType<KSystemClockMonitor>(uri, 1, 0, "SystemClockMonitor");
}

void TimedWallpaperExtensionPlugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
    Q_UNUSED(uri)
    engine->addImageProvider(QLatin1String("timed"), new TimedWallpaperImageProvider);
    engine->addImageProvider(QLatin1String("timedpreview"), new TimedWallpaperPreviewProvider);
}
