/*
 * SPDX-FileCopyrightText: 2020 Vlad Zahorodnii <vlad.zahorodnii@kde.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config-timedwallpaper.h"

#include "timedwallpaperhandler.h"
#include "timedwallpaperdescription.h"
#include "timedwallpaperengine_solar.h"
#include "timedwallpaperengine_timed.h"

#include <KConfigGroup>
#include <KLocalizedString>
#include <KPackage/PackageLoader>
#include <KSharedConfig>

TimedWallpaperHandler::TimedWallpaperHandler(QObject *parent)
    : QObject(parent)
    , m_updateTimer(new QTimer(this))
{
    m_updateTimer->setInterval(0);
    m_updateTimer->setSingleShot(true);
    connect(m_updateTimer, &QTimer::timeout, this, &TimedWallpaperHandler::update);
}

TimedWallpaperHandler::~TimedWallpaperHandler()
{
    delete m_engine;
}

void TimedWallpaperHandler::setLocation(const QGeoCoordinate &coordinate)
{
    if (m_location == coordinate)
        return;
    m_location = coordinate;
    reloadEngine();
    scheduleUpdate();
    emit locationChanged();
}

QGeoCoordinate TimedWallpaperHandler::location() const
{
    return m_location;
}

static QUrl locateWallpaper(const QString &name)
{
    const QString packagePath = QStandardPaths::locate(QStandardPaths::GenericDataLocation,
                                                       QStringLiteral("wallpapers/") + name,
                                                       QStandardPaths::LocateDirectory);

    KPackage::PackageLoader *packageLoader = KPackage::PackageLoader::self();
    KPackage::Package package = packageLoader->loadPackage(QStringLiteral("Wallpaper/Timed"));
    package.setPath(packagePath);
    if (package.isValid())
        return package.fileUrl(QByteArrayLiteral("timed"));

    const QString filePath = QStandardPaths::locate(QStandardPaths::GenericDataLocation,
                                                    QStringLiteral("wallpapers/") + name,
                                                    QStandardPaths::LocateFile);

    return QUrl::fromLocalFile(filePath);
}

static QUrl defaultLookAndFeelWallpaper()
{
    KConfigGroup kdeConfigGroup(KSharedConfig::openConfig(QStringLiteral("kdeglobals")), "KDE");
    const QString lookAndFeelPackageName = kdeConfigGroup.readEntry("LookAndFeelPackage");

    KPackage::PackageLoader *packageLoader = KPackage::PackageLoader::self();
    KPackage::Package lookAndFeelPackage =
            packageLoader->loadPackage(QStringLiteral("Plasma/LookAndFeel"));
    if (!lookAndFeelPackageName.isEmpty())
        lookAndFeelPackage.setPath(lookAndFeelPackageName);

    KSharedConfigPtr lookAndFeelConfig =
            KSharedConfig::openConfig(lookAndFeelPackage.filePath("defaults"));
    KConfigGroup wallpaperConfigGroup = KConfigGroup(lookAndFeelConfig, "Timed Wallpaper");

    const QString wallpaperName = wallpaperConfigGroup.readEntry("Image");
    if (wallpaperName.isEmpty())
        return QUrl();

    return locateWallpaper(wallpaperName);
}

static QUrl defaultFallbackWallpaper()
{
    return locateWallpaper(QStringLiteral(FALLBACK_WALLPAPER));
}

static QUrl defaultWallpaper()
{
    QUrl fileUrl = defaultLookAndFeelWallpaper();
    if (fileUrl.isValid())
        return fileUrl;
    return defaultFallbackWallpaper();
}

void TimedWallpaperHandler::setSource(const QUrl &url)
{
    const QUrl source = url.isValid() ? url : defaultWallpaper();
    if (m_source == source)
        return;
    m_source = source;
    reloadDescription();
    reloadEngine();
    scheduleUpdate();
    emit sourceChanged();
}

QUrl TimedWallpaperHandler::source() const
{
    return m_source;
}

void TimedWallpaperHandler::setTopLayer(const QUrl &url)
{
    if (m_topLayer == url)
        return;
    m_topLayer = url;
    emit topLayerChanged();
}

QUrl TimedWallpaperHandler::topLayer() const
{
    return m_topLayer;
}

void TimedWallpaperHandler::setBottomLayer(const QUrl &url)
{
    if (m_bottomLayer == url)
        return;
    m_bottomLayer = url;
    emit bottomLayerChanged();
}

QUrl TimedWallpaperHandler::bottomLayer() const
{
    return m_bottomLayer;
}

void TimedWallpaperHandler::setBlendFactor(qreal blendFactor)
{
    if (m_blendFactor == blendFactor)
        return;
    m_blendFactor = blendFactor;
    emit blendFactorChanged();
}

qreal TimedWallpaperHandler::blendFactor() const
{
    return m_blendFactor;
}

void TimedWallpaperHandler::setStatus(Status status)
{
    if (m_status == status)
        return;
    m_status = status;
    emit statusChanged();
}

TimedWallpaperHandler::Status TimedWallpaperHandler::status() const
{
    return m_status;
}

void TimedWallpaperHandler::setErrorString(const QString &text)
{
    if (m_errorString == text)
        return;
    m_errorString = text;
    emit errorStringChanged();
}

QString TimedWallpaperHandler::errorString() const
{
    return m_errorString;
}

void TimedWallpaperHandler::scheduleUpdate()
{
    m_updateTimer->start();
}

void TimedWallpaperHandler::update()
{
    if (m_status != Ready)
        return;
    if (!m_engine || m_engine->isExpired())
        reloadEngine();
    m_engine->update();
    setTopLayer(m_engine->topLayer());
    setBottomLayer(m_engine->bottomLayer());
    setBlendFactor(m_engine->blendFactor());
}

void TimedWallpaperHandler::reloadDescription()
{
    const QString fileName = m_source.toLocalFile();

    m_description = TimedWallpaperDescription::fromFile(fileName);

    if (m_description.isValid()) {
        setStatus(Ready);
    } else {
        setErrorString(i18n("%1 is not a timed wallpaper", fileName));
        setStatus(Error);
    }
}

void TimedWallpaperHandler::reloadEngine()
{
    delete m_engine;
    m_engine = nullptr;

    if (!m_description.isValid())
        return;

    if (m_description.supportedEngines() & TimedWallpaperDescription::SolarEngine)
        m_engine = TimedWallpaperEngineSolar::create(m_location);
    if (!m_engine)
        m_engine = TimedWallpaperEngineTimed::create();

    m_engine->setDescription(m_description);
}
