/*
 * SPDX-FileCopyrightText: 2020 Vlad Zahorodnii <vlad.zahorodnii@kde.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "timedwallpaperimageprovider.h"
#include "timedwallpaperimagehandle.h"

#include <KTimedWallpaperReader>

#include <QDebug>

TimedWallpaperImageProvider::TimedWallpaperImageProvider()
    : QQuickImageProvider(QQuickImageProvider::Image)
{
}

QImage TimedWallpaperImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    const TimedWallpaperImageHandle handle = TimedWallpaperImageHandle::fromString(id);
    if (!handle.isValid()) {
        qWarning() << "Failed to decode a TimedWallpaperImageHandle from" << id;
        return QImage();
    }

    KTimedWallpaperReader reader(handle.fileName());

    QImage image = reader.imageAt(handle.imageIndex());
    if (reader.error() != KTimedWallpaperReader::NoError) {
        qWarning("Failed to read %s: %s", qPrintable(reader.fileName()), qPrintable(reader.errorString()));
        return QImage();
    }

    *size = image.size();

    // If the sourceSize is set, scale the image to that size and ignore the aspect ratio.
    if (requestedSize.isValid())
        image = image.scaled(requestedSize);

    return image;
}
