/*
 * SPDX-FileCopyrightText: 2020 Vlad Zahorodnii <vlad.zahorodnii@kde.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "timedwallpapermodel.h"
#include "timedwallpapercrawler.h"

#include <KAboutData>
#include <KConfigGroup>
#include <KPackage/PackageLoader>
#include <KSharedConfig>

#include <QFileInfo>
#include <QStandardPaths>

// TODO: The model can be implemented better.

class TimedWallpaper
{
public:
    static TimedWallpaper *fromFile(const QUrl &fileUrl);
    static TimedWallpaper *fromPackage(const QUrl &packageUrl);

    QUrl imageUrl;
    QUrl folderUrl;
    QUrl previewUrl;
    QString name;
    QString packageName;
    QString license;
    QString author;
    bool isPackage = false;
    bool isCustom = false;
    bool isRemovable = false;
    bool isZombie = false;
};

static QUrl folderUrlForImageUrl(const QUrl &url)
{
    const QString fileName = url.toLocalFile();
    const QFileInfo fileInfo(fileName);
    return QUrl::fromLocalFile(fileInfo.path());
}

static QUrl previewUrlForImageUrl(const QUrl &url)
{
    const QString fileName = url.toLocalFile();
    const QString base64 = fileName.toUtf8().toBase64();
    return QLatin1String("image://timedpreview/") + base64;
}

static bool checkRemovable(const QUrl &url)
{
    const QString fileName = url.toLocalFile();
    const QString dataDirectory = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation);
    return fileName.startsWith(dataDirectory);
}

TimedWallpaper *TimedWallpaper::fromFile(const QUrl &fileUrl)
{
    TimedWallpaper *wallpaper = new TimedWallpaper;
    wallpaper->imageUrl = fileUrl;
    wallpaper->folderUrl = folderUrlForImageUrl(fileUrl);
    wallpaper->previewUrl = previewUrlForImageUrl(fileUrl);
    wallpaper->name = fileUrl.fileName(QUrl::PrettyDecoded);
    return wallpaper;
}

TimedWallpaper *TimedWallpaper::fromPackage(const QUrl &packageUrl)
{
    KPackage::Package package =
            KPackage::PackageLoader::self()->loadPackage(QStringLiteral("Wallpaper/Timed"));
    package.setPath(packageUrl.toLocalFile());

    const QUrl fileUrl = package.fileUrl(QByteArrayLiteral("timed"));
    const KPluginMetaData metaData = package.metadata();

    TimedWallpaper *wallpaper = new TimedWallpaper;
    wallpaper->imageUrl = fileUrl;
    wallpaper->folderUrl = folderUrlForImageUrl(fileUrl);
    wallpaper->previewUrl = previewUrlForImageUrl(fileUrl);
    wallpaper->name = metaData.name();
    wallpaper->packageName = metaData.pluginId();
    wallpaper->license = metaData.license();
    wallpaper->isPackage = true;

    if (!metaData.authors().isEmpty())
        wallpaper->author = metaData.authors().first().name();

    return wallpaper;
}

class TimedWallpaperModelPrivate : public QObject
{
    Q_OBJECT

public:
    TimedWallpaperModelPrivate(TimedWallpaperModel *model);

    TimedWallpaper *wallpaperForIndex(const QModelIndex &index) const;

    void internalAppend(TimedWallpaper *wallpaper);
    void internalPrepend(TimedWallpaper *wallpaper);
    void internalScheduleRemove(const QModelIndex &index, bool set);
    void internalRemove(const QModelIndex &index);
    void internalReset();

    bool contains(const QUrl &fileUrl) const;
    QModelIndex find(const QUrl &fileUrl) const;

    bool registerFileName(const QString &fileName);
    void unregisterFileName(const QString &fileName);

    void addCustomWallpaper(const QUrl &fileUrl);
    void addFileWallpaper(const QUrl &fileUrl);
    void addPackageWallpaper(const QUrl &folderUrl);

    void removeCustomWallpaper(const QModelIndex &index);
    void removeFileWallpaper(const QModelIndex &index);
    void removePackageWallpaper(const QModelIndex &index);

    void loadCustomWallpapers();
    void loadGenericWallpapers();

    void handleFoundPackage(const QString &packagePath, const QUuid &token);
    void handleFoundFile(const QString &filePath, const QUuid &token);

    TimedWallpaperModel *q;
    QVector<TimedWallpaper *> wallpapers;
    KSharedConfigPtr config;
    QPointer<TimedWallpaperCrawler> crawler;
    QUuid lastToken;
};

TimedWallpaperModelPrivate::TimedWallpaperModelPrivate(TimedWallpaperModel *model)
    : q(model)
    , config(KSharedConfig::openConfig(QStringLiteral("ktimedwallpaperrc")))
{
}

TimedWallpaper *TimedWallpaperModelPrivate::wallpaperForIndex(const QModelIndex &index) const
{
    if (!index.isValid())
        return nullptr;
    return wallpapers.value(index.row());
}

void TimedWallpaperModelPrivate::internalAppend(TimedWallpaper *wallpaper)
{
    const int row = wallpapers.count();

    q->beginInsertRows(QModelIndex(), row, row);
    wallpapers.append(wallpaper);
    q->endInsertRows();
}

void TimedWallpaperModelPrivate::internalPrepend(TimedWallpaper *wallpaper)
{
    q->beginInsertRows(QModelIndex(), 0, 0);
    wallpapers.prepend(wallpaper);
    q->endInsertRows();
}

void TimedWallpaperModelPrivate::internalScheduleRemove(const QModelIndex &index, bool set)
{
    const int row = index.row();

    if (wallpapers[row]->isZombie == set)
        return;
    wallpapers[row]->isZombie = set;
    emit q->dataChanged(index, index, { TimedWallpaperModel::WallpaperIsZombieRole });
}

void TimedWallpaperModelPrivate::internalRemove(const QModelIndex &index)
{
    const int row = index.row();

    q->beginRemoveRows(QModelIndex(), row, row);
    delete wallpapers.takeAt(row);
    q->endRemoveRows();
}

void TimedWallpaperModelPrivate::internalReset()
{
    q->beginResetModel();
    qDeleteAll(wallpapers);
    wallpapers.clear();
    q->endResetModel();
}

bool TimedWallpaperModelPrivate::contains(const QUrl &fileUrl) const
{
    return find(fileUrl).isValid();
}

QModelIndex TimedWallpaperModelPrivate::find(const QUrl &fileUrl) const
{
    for (int i = 0; i < wallpapers.count(); ++i) {
        if (wallpapers[i]->imageUrl == fileUrl)
            return q->createIndex(i, 0);
    }

    return QModelIndex();
}

bool TimedWallpaperModelPrivate::registerFileName(const QString &fileName)
{
    KConfigGroup group(config, QStringLiteral("General"));
    QStringList wallpapers = group.readEntry(QStringLiteral("Wallpapers"), QStringList());

    if (wallpapers.contains(fileName))
        return false;

    wallpapers.prepend(fileName);

    group.writeEntry(QStringLiteral("Wallpapers"), wallpapers);
    group.sync();

    return true;
}

void TimedWallpaperModelPrivate::unregisterFileName(const QString &fileName)
{
    KConfigGroup group(config, QStringLiteral("General"));
    QStringList wallpapers = group.readEntry(QStringLiteral("Wallpapers"), QStringList());

    wallpapers.removeOne(fileName);

    group.writeEntry(QStringLiteral("Wallpapers"), wallpapers);
    group.sync();
}

void TimedWallpaperModelPrivate::addCustomWallpaper(const QUrl &fileUrl)
{
    const QString fileName = fileUrl.toLocalFile();
    if (fileName.isEmpty())
        return;

    if (!registerFileName(fileName))
        return;

    TimedWallpaper *wallpaper = TimedWallpaper::fromFile(fileUrl);
    wallpaper->isRemovable = true;
    wallpaper->isCustom = true;

    internalPrepend(wallpaper);
}

void TimedWallpaperModelPrivate::addFileWallpaper(const QUrl &fileUrl)
{
    TimedWallpaper *wallpaper = TimedWallpaper::fromFile(fileUrl);
    wallpaper->isRemovable = checkRemovable(fileUrl);

    internalAppend(wallpaper);
}

void TimedWallpaperModelPrivate::addPackageWallpaper(const QUrl &folderUrl)
{
    TimedWallpaper *wallpaper = TimedWallpaper::fromPackage(folderUrl);
    wallpaper->isRemovable = checkRemovable(folderUrl);

    internalAppend(wallpaper);
}

void TimedWallpaperModelPrivate::removeCustomWallpaper(const QModelIndex &index)
{
    const TimedWallpaper *wallpaper = wallpaperForIndex(index);

    unregisterFileName(wallpaper->imageUrl.toLocalFile());
    internalRemove(index);
}

void TimedWallpaperModelPrivate::removeFileWallpaper(const QModelIndex &index)
{
    const TimedWallpaper *wallpaper = wallpaperForIndex(index);

    if (!QFile::remove(wallpaper->imageUrl.toLocalFile()))
        return;

    internalRemove(index);
}

void TimedWallpaperModelPrivate::removePackageWallpaper(const QModelIndex &index)
{
    const TimedWallpaper *wallpaper = wallpaperForIndex(index);
    const QUrl imageUrl = wallpaper->imageUrl;

    const QString dataLocation = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation);
    const QString wallpaperPackageRoot = dataLocation + QStringLiteral("/wallpapers/");
    KPackage::Package package =
            KPackage::PackageLoader::self()->loadPackage(QStringLiteral("Wallpaper/Timed"));

    KJob *uninstallJob = package.uninstall(wallpaper->packageName, wallpaperPackageRoot);

    connect(uninstallJob, &KJob::finished, this, [this, imageUrl](KJob *job) {
        if (job->error() != KJob::NoError)
            return;
        const QModelIndex index = find(imageUrl);
        if (!index.isValid())
            return;
        internalRemove(index);
    });
}

void TimedWallpaperModelPrivate::loadCustomWallpapers()
{
    KConfigGroup group(config, QStringLiteral("General"));
    const QStringList wallpaperFileNames = group.readEntry("Wallpapers", QStringList());

    for (const QString &wallpaperFileName : wallpaperFileNames) {
        const QUrl wallpaperUrl = QUrl::fromUserInput(wallpaperFileName);
        if (contains(wallpaperUrl))
            continue;

        TimedWallpaper *wallpaper = TimedWallpaper::fromFile(wallpaperUrl);
        wallpaper->isRemovable = true;
        wallpaper->isCustom = true;

        internalAppend(wallpaper);
    }
}

void TimedWallpaperModelPrivate::loadGenericWallpapers()
{
    QStringList candidates = QStandardPaths::locateAll(QStandardPaths::GenericDataLocation,
                                                       QStringLiteral("wallpapers"),
                                                       QStandardPaths::LocateDirectory);

    // Load the package structure in the main thread because it seems like the PackageLoader
    // class is not thread-safe. Notice that system wallpapers are discovered in another thread
    // since we may need to read file contents in order to determine whether a given file is
    // actually a timed wallpaper and not just some random heif file.
    KPackage::PackageStructure *packageStructure =
            KPackage::PackageLoader::self()->loadPackageStructure(QStringLiteral("Wallpaper/Timed"));

    TimedWallpaperCrawler *crawler = new TimedWallpaperCrawler(this);
    connect(crawler, &TimedWallpaperCrawler::foundFile,
            this, &TimedWallpaperModelPrivate::handleFoundFile);
    connect(crawler, &TimedWallpaperCrawler::foundPackage,
            this, &TimedWallpaperModelPrivate::handleFoundPackage);

    crawler->setSearchRoots(candidates);
    crawler->setPackageStructure(packageStructure);
    crawler->start(QThread::LowPriority);

    // Queued events are delivered no matter what, except the case where the receiver object
    // is destroyed. So each crawler has a token that uniquely identifies it. We use the token
    // to filter out timed wallpapers that are discovered by the previous crawler, if there
    // is any.
    lastToken = crawler->token();
}

void TimedWallpaperModelPrivate::handleFoundFile(const QString &packagePath, const QUuid &token)
{
    if (lastToken == token)
        addFileWallpaper(QUrl::fromLocalFile(packagePath));
}

void TimedWallpaperModelPrivate::handleFoundPackage(const QString &filePath, const QUuid &token)
{
    if (lastToken == token)
        addPackageWallpaper(QUrl::fromLocalFile(filePath));
}

/**
 * Constructs an empty TimedWallpaperModel object.
 */
TimedWallpaperModel::TimedWallpaperModel(QObject *parent)
    : QAbstractListModel(parent)
    , d(new TimedWallpaperModelPrivate(this))
{
}

/**
 * Destructs the TimedWallpaperModel object.
 */
TimedWallpaperModel::~TimedWallpaperModel()
{
    qDeleteAll(d->wallpapers);
}

QHash<int, QByteArray> TimedWallpaperModel::roleNames() const
{
    QHash<int, QByteArray> roleNames = QAbstractListModel::roleNames();
    roleNames.insert(WallpaperNameRole, QByteArrayLiteral("name"));
    roleNames.insert(WallpaperFolderRole, QByteArrayLiteral("folder"));
    roleNames.insert(WallpaperLicenseRole, QByteArrayLiteral("license"));
    roleNames.insert(WallpaperAuthorRole, QByteArrayLiteral("author"));
    roleNames.insert(WallpaperIsPackageRole, QByteArrayLiteral("package"));
    roleNames.insert(WallpaperIsCustomRole, QByteArrayLiteral("custom"));
    roleNames.insert(WallpaperIsRemovableRole, QByteArrayLiteral("removable"));
    roleNames.insert(WallpaperIsZombieRole, QByteArrayLiteral("zombie"));
    roleNames.insert(WallpaperImageRole, QByteArrayLiteral("image"));
    roleNames.insert(WallpaperPreviewRole, QByteArrayLiteral("preview"));
    return roleNames;
}

/**
 * Reimplemented to return the total number of timed wallpapers in the model.
 */
int TimedWallpaperModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return d->wallpapers.count();
}

QVariant TimedWallpaperModel::data(const QModelIndex &index, int role) const
{
    const TimedWallpaper *wallpaper = d->wallpaperForIndex(index);
    if (!wallpaper)
        return QVariant();

    switch (role) {
    case Qt::DisplayRole:
    case WallpaperNameRole:
        return wallpaper->name;
    case WallpaperFolderRole:
        return wallpaper->folderUrl;
    case WallpaperLicenseRole:
        return wallpaper->license;
    case WallpaperAuthorRole:
        return wallpaper->author;
    case WallpaperIsPackageRole:
        return wallpaper->isPackage;
    case WallpaperIsCustomRole:
        return wallpaper->isCustom;
    case WallpaperIsRemovableRole:
        return wallpaper->isRemovable;
    case WallpaperIsZombieRole:
        return wallpaper->isZombie;
    case WallpaperImageRole:
        return wallpaper->imageUrl;
    case WallpaperPreviewRole:
        return wallpaper->previewUrl;
    }

    return QVariant();
}

/**
 * Returns the index of the timed wallpaper identified by the specified image url \p fileUrl.
 *
 * This method will return -1 if timed wallpaper with the given image url doesn't exist.
 */
int TimedWallpaperModel::find(const QUrl &fileUrl) const
{
    const QModelIndex index = d->find(fileUrl);
    return index.isValid() ? index.row() : -1;
}

/**
 * Returns a QModelIndex for the specified \p index.
 */
QModelIndex TimedWallpaperModel::modelIndex(int index) const
{
    return createIndex(index, 0);
}

/**
 * Reloads the timed wallpaper model.
 */
void TimedWallpaperModel::reload()
{
    d->config->markAsClean();
    d->config->reparseConfiguration();

    d->internalReset();
    d->loadCustomWallpapers();
    d->loadGenericWallpapers();
}

/**
 * Removes all timed wallpapers that are scheduled to be removed.
 */
void TimedWallpaperModel::purge()
{
    for (int i = rowCount() - 1; i >= 0; --i) {
        const QModelIndex wallpaperIndex = createIndex(i, 0);
        if (!wallpaperIndex.data(WallpaperIsZombieRole).toBool())
            continue;
        remove(wallpaperIndex);
    }
}

/**
 * Adds the timed wallpaper with the given url \p fileUrl to the model.
 */
void TimedWallpaperModel::add(const QUrl &fileUrl)
{
    d->addCustomWallpaper(fileUrl);
}

/**
 * Schedules a timed wallpaper with the specified model index \p index for removal.
 *
 * The timed wallpaper will be removed from the model when purge() is called.
 */
void TimedWallpaperModel::scheduleRemove(const QModelIndex &index)
{
    if (!index.isValid())
        return;

    d->internalScheduleRemove(index, true);
}

/**
 * Unschedules a timed wallpaper with the specified model index \p index for removal.
 */
void TimedWallpaperModel::unscheduleRemove(const QModelIndex &index)
{
    if (!index.isValid())
        return;

    d->internalScheduleRemove(index, false);
}

/**
 * Removes the timed wallpaper with the specified model index \p index.
 */
void TimedWallpaperModel::remove(const QModelIndex &index)
{
    const TimedWallpaper *wallpaper = d->wallpaperForIndex(index);
    if (!wallpaper)
        return;

    if (wallpaper->isCustom)
        d->removeCustomWallpaper(index);
    else if (wallpaper->isPackage)
        d->removePackageWallpaper(index);
    else
        d->removeFileWallpaper(index);
}

#include "timedwallpapermodel.moc"
