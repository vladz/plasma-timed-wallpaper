/*
 * SPDX-FileCopyrightText: 2020 Vlad Zahorodnii <vlad.zahorodnii@kde.org>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "ktimedwallpapermetadata.h"

#include <QDomDocument>
#include <QDomNode>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QSharedData>

/**
 * \class KTimedWallpaperMetaData
 * \brief The KTimedWallpaperMetaData class represents metadata associated with images
 * in the timed wallpaper.
 *
 * KTimedWallpaperMetaData provides information about images in the timed wallpaper,
 * for example, solar position, etc. Some fields may not be specified. In order to check
 * whether the given field is set, test the corresponding bit in fields().
 */

static QJsonValue crossFadeModeToJson(KTimedWallpaperMetaData::CrossFadeMode crossFadeMode)
{
    switch (crossFadeMode) {
    case KTimedWallpaperMetaData::NoCrossFade:
        return QJsonValue(false);
    case KTimedWallpaperMetaData::CrossFade:
        return QJsonValue(true);
    default:
        Q_UNREACHABLE();
    }
}

static KTimedWallpaperMetaData::CrossFadeMode crossFadeModeFromJson(const QJsonValue &value)
{
    return value.toBool() ? KTimedWallpaperMetaData::CrossFade : KTimedWallpaperMetaData::NoCrossFade;
}

class KTimedWallpaperMetaDataPrivate : public QSharedData
{
public:
    KTimedWallpaperMetaDataPrivate();

    KTimedWallpaperMetaData::MetaDataFields presentFields;
    KTimedWallpaperMetaData::CrossFadeMode crossFadeMode;
    qreal solarAzimuth;
    qreal solarElevation;
    qreal time;
};

KTimedWallpaperMetaDataPrivate::KTimedWallpaperMetaDataPrivate()
    : crossFadeMode(KTimedWallpaperMetaData::NoCrossFade)
    , solarAzimuth(0.0)
    , solarElevation(0.0)
    , time(0.0)
{
}

/**
 * Constructs an empty KTimedWallpaperMetaData object.
 */
KTimedWallpaperMetaData::KTimedWallpaperMetaData()
    : d(new KTimedWallpaperMetaDataPrivate)
{
}

/**
 * Constructs a copy of the KTimedWallpaperMetaData object.
 */
KTimedWallpaperMetaData::KTimedWallpaperMetaData(const KTimedWallpaperMetaData &other)
    : d(other.d)
{
}

/**
 * Destructs the KTimedWallpaperMetaData object.
 */
KTimedWallpaperMetaData::~KTimedWallpaperMetaData()
{
}

/**
 * Assigns the value of \p other to a timed wallpaper metadata object.
 */
KTimedWallpaperMetaData &KTimedWallpaperMetaData::operator=(const KTimedWallpaperMetaData &other)
{
    d = other.d;
    return *this;
}

/**
 * Returns a bitmask that indicates which fields are present in the metadata.
 */
KTimedWallpaperMetaData::MetaDataFields KTimedWallpaperMetaData::fields() const
{
    return d->presentFields;
}

/**
 * Returns \c true if the KTimedWallpaperMetaData contains valid metadata; otherwise \c false.
 */
bool KTimedWallpaperMetaData::isValid() const
{
    const MetaDataFields requiredFields = TimeField;
    if ((d->presentFields & requiredFields) != requiredFields)
        return false;

    if (bool(d->presentFields & SolarAzimuthField) ^ bool(d->presentFields & SolarElevationField))
        return false;

    if (d->time < 0 || d->time > 1)
        return false;

    return true;
}

/**
 * Sets the value of the cross-fade mode field to \p mode.
 */
void KTimedWallpaperMetaData::setCrossFadeMode(CrossFadeMode mode)
{
    d->crossFadeMode = mode;
    d->presentFields |= CrossFadeField;
}

/**
 * Returns the value of the cross-fade mode field in the timed wallpaper metadata.
 */
KTimedWallpaperMetaData::CrossFadeMode KTimedWallpaperMetaData::crossFadeMode() const
{
    return d->crossFadeMode;
}

/**
 * Sets the value of the time field to \p time.
 */
void KTimedWallpaperMetaData::setTime(qreal time)
{
    d->time = time;
    d->presentFields |= TimeField;
}

/**
 * Returns the value of the time field in the timed wallpaper metadata.
 */
qreal KTimedWallpaperMetaData::time() const
{
    return d->time;
}

/**
 * Sets the value of the solar elevation field to \p elevation.
 */
void KTimedWallpaperMetaData::setSolarElevation(qreal elevation)
{
    d->solarElevation = elevation;
    d->presentFields |= SolarElevationField;
}

/**
 * Returns the value of solar elevation stored in the timed wallpaper metadata.
 *
 * Note that this method will return \c 0 if SolarElevationField is not set in fields().
 */
qreal KTimedWallpaperMetaData::solarElevation() const
{
    return d->solarElevation;
}

/**
 * Sets the value of the solar azimuth field to \p azimuth.
 */
void KTimedWallpaperMetaData::setSolarAzimuth(qreal azimuth)
{
    d->solarAzimuth = azimuth;
    d->presentFields |= SolarAzimuthField;
}

/**
 * Returns the value of solar azimuth stored in the timed wallpaper metadata.
 *
 * Note that this method will return \c 0 if SolarAzimuthField is not set in fields().
 */
qreal KTimedWallpaperMetaData::solarAzimuth() const
{
    return d->solarAzimuth;
}

/**
 * Converts the KTimedWallpaperMetaData to a UTF-8 encoded JSON document.
 *
 * This method returns an empty QByteArray if the metadata is invalid.
 */
QByteArray KTimedWallpaperMetaData::toJson() const
{
    if (!isValid())
        return QByteArray();

    QJsonObject rootObject;

    if (d->presentFields & CrossFadeField)
        rootObject[QLatin1String("CrossFade")] = crossFadeModeToJson(d->crossFadeMode);
    if (d->presentFields & SolarElevationField)
        rootObject[QLatin1String("Elevation")] = d->solarElevation;
    if (d->presentFields & SolarAzimuthField)
        rootObject[QLatin1String("Azimuth")] = d->solarAzimuth;
    rootObject[QLatin1String("Time")] = d->time;

    QJsonDocument document;
    document.setObject(rootObject);

    return document.toJson(QJsonDocument::Compact);
}

/**
 * Converts the KTimedWallpaperMetaData to a Base64 string.
 */
QByteArray KTimedWallpaperMetaData::toBase64() const
{
    return toJson().toBase64();
}

/**
 * Converts the KTimedWallpaperMetaData to a Base64 string, and stores it in XMP metadata.
 */
QByteArray KTimedWallpaperMetaData::toXmp() const
{
    if (!isValid())
        return QByteArray();

    QFile xmpTemplateFile(QStringLiteral(":/ktimedwallpaper/xmp/metadata.xml"));
    xmpTemplateFile.open(QFile::ReadOnly);

    QByteArray xmpMetaData = xmpTemplateFile.readAll();
    xmpMetaData.replace(QByteArrayLiteral("base64"), toBase64());

    return xmpMetaData;
}

/**
 * Decodes a JSON-encoded KTimedWallpaperMetaData object.
 */
KTimedWallpaperMetaData KTimedWallpaperMetaData::fromJson(const QByteArray &json)
{
    KTimedWallpaperMetaData metaData;

    const QJsonDocument document = QJsonDocument::fromJson(json);
    if (document.isNull())
        return metaData;

    const QJsonObject rootObject = document.object();
    if (rootObject.isEmpty())
        return metaData;

    const QJsonValue crossFadeMode = rootObject[QLatin1String("CrossFade")];
    if (crossFadeMode.isBool())
        metaData.setCrossFadeMode(crossFadeModeFromJson(crossFadeMode));

    const QJsonValue time = rootObject[QLatin1String("Time")];
    if (time.isDouble())
        metaData.setTime(time.toDouble());

    const QJsonValue solarElevation = rootObject[QLatin1String("Elevation")];
    if (solarElevation.isDouble())
        metaData.setSolarElevation(solarElevation.toDouble());

    const QJsonValue solarAzimuth = rootObject[QLatin1String("Azimuth")];
    if (solarAzimuth.isDouble())
        metaData.setSolarAzimuth(solarAzimuth.toDouble());

    return metaData;
}

/**
 * Decodes a Base64-encoded KTimedWallpaperMetaData object.
 */
KTimedWallpaperMetaData KTimedWallpaperMetaData::fromBase64(const QByteArray &base64)
{
    return fromJson(QByteArray::fromBase64(base64));
}

/**
 * Creates a KTimedWallpaperMetaData object from the specified \p xmp metadata.
 */
KTimedWallpaperMetaData KTimedWallpaperMetaData::fromXmp(const QByteArray &xmp)
{
    QDomDocument xmpDocument;
    xmpDocument.setContent(xmp);
    if (xmpDocument.isNull())
        return KTimedWallpaperMetaData();

    const QString attributeName = QStringLiteral("plasma:TimedWallpaper");

    const QDomNodeList descriptionNodes = xmpDocument.elementsByTagName(QStringLiteral("rdf:Description"));
    for (int i = 0; i < descriptionNodes.count(); ++i) {
        QDomElement descriptionNode = descriptionNodes.at(i).toElement();
        const QString encodedMetaData = descriptionNode.attribute(attributeName);
        if (encodedMetaData.isEmpty())
            continue;
        KTimedWallpaperMetaData metaData = fromBase64(encodedMetaData.toUtf8());
        if (metaData.isValid())
            return metaData;
    }

    return KTimedWallpaperMetaData();
}
