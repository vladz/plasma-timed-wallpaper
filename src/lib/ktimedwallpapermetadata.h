/*
 * SPDX-FileCopyrightText: 2020 Vlad Zahorodnii <vlad.zahorodnii@kde.org>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "ktimedwallpaper_export.h"

#include <QByteArray>
#include <QSharedDataPointer>

class KTimedWallpaperMetaDataPrivate;

class KTIMEDWALLPAPER_EXPORT KTimedWallpaperMetaData
{
public:
    enum CrossFadeMode {
        NoCrossFade,
        CrossFade,
    };

    enum MetaDataField {
        CrossFadeField          = 1 << 0,
        TimeField               = 1 << 1,
        SolarAzimuthField       = 1 << 2,
        SolarElevationField     = 1 << 3,
    };
    Q_DECLARE_FLAGS(MetaDataFields, MetaDataField)

    KTimedWallpaperMetaData();
    KTimedWallpaperMetaData(const KTimedWallpaperMetaData &other);
    ~KTimedWallpaperMetaData();

    KTimedWallpaperMetaData &operator=(const KTimedWallpaperMetaData &other);

    MetaDataFields fields() const;
    bool isValid() const;

    void setCrossFadeMode(CrossFadeMode mode);
    CrossFadeMode crossFadeMode() const;

    void setTime(qreal time);
    qreal time() const;

    void setSolarElevation(qreal elevation);
    qreal solarElevation() const;

    void setSolarAzimuth(qreal azimuth);
    qreal solarAzimuth() const;

    QByteArray toJson() const;
    QByteArray toBase64() const;
    QByteArray toXmp() const;

    static KTimedWallpaperMetaData fromJson(const QByteArray &json);
    static KTimedWallpaperMetaData fromBase64(const QByteArray &base64);
    static KTimedWallpaperMetaData fromXmp(const QByteArray &xmp);

private:
    QSharedDataPointer<KTimedWallpaperMetaDataPrivate> d;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(KTimedWallpaperMetaData::MetaDataFields)
