/*
 * SPDX-FileCopyrightText: 2020 Vlad Zahorodnii <vlad.zahorodnii@kde.org>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include "ktimedwallpaper_export.h"

#include <QIODevice>

class KTimedWallpaperMetaData;
class KTimedWallpaperReaderPrivate;

class KTIMEDWALLPAPER_EXPORT KTimedWallpaperReader
{
public:
    enum WallpaperReaderError {
        NoError,
        DeviceError,
        InvalidDataError,
        NoMetaDataError,
        UnknownError,
    };

    KTimedWallpaperReader();
    explicit KTimedWallpaperReader(QIODevice *device);
    explicit KTimedWallpaperReader(const QString &fileName);
    ~KTimedWallpaperReader();

    void setDevice(QIODevice *device);
    QIODevice *device() const;

    void setFileName(const QString &fileName);
    QString fileName() const;

    int imageCount() const;

    KTimedWallpaperMetaData metaDataAt(int imageIndex) const;
    QImage imageAt(int imageIndex) const;

    WallpaperReaderError error() const;
    QString errorString() const;

    static bool canRead(QIODevice *device);
    static bool canRead(const QString &fileName);

private:
    QScopedPointer<KTimedWallpaperReaderPrivate> d;
};
