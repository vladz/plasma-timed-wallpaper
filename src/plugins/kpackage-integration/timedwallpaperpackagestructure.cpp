/*
 * SPDX-FileCopyrightText: 2020 Vlad Zahorodnii <vlad.zahorodnii@kde.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "timedwallpaperpackagestructure.h"

#include <KLocalizedString>

#include <QFileInfo>

TimedWallpaperPackageStructure::TimedWallpaperPackageStructure(QObject *parent, const QVariantList &args)
    : KPackage::PackageStructure(parent, args)
{
}

void TimedWallpaperPackageStructure::initPackage(KPackage::Package *package)
{
    package->addDirectoryDefinition(QByteArrayLiteral("images"),
                                    QStringLiteral("images/"),
                                    i18n("Timed wallpaper files"));
    package->setRequired(QByteArrayLiteral("images"), true);
}

void TimedWallpaperPackageStructure::pathChanged(KPackage::Package *package)
{
    package->removeDefinition(QByteArrayLiteral("timed"));

    const QStringList fileFormats { QStringLiteral(".heic"), QStringLiteral(".heif") };

    for (const QString &fileFormat : fileFormats) {
        const QFileInfo fileInfo(package->path() + QLatin1String("contents/images/timed") + fileFormat);
        if (!fileInfo.exists())
            continue;
        package->addFileDefinition(QByteArrayLiteral("timed"),
                                   QStringLiteral("images/timed") + fileFormat,
                                   i18n("Timed wallpaper file"));
        package->setRequired(QByteArrayLiteral("timed"), true);
        break;
    }
}

K_EXPORT_KPACKAGE_PACKAGE_WITH_JSON(TimedWallpaperPackageStructure, "timedwallpaperpackagestructure.json")

#include "timedwallpaperpackagestructure.moc"
