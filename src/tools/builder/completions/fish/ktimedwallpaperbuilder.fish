# ktimedwallpaperbuilder(1) completion

complete -f -c ktimedwallpaperbuilder
complete -c ktimedwallpaperbuilder -s v -l version -d "Print the version information and quit"
complete -c ktimedwallpaperbuilder -s h -l help -d "Show help message and quit"
complete -c ktimedwallpaperbuilder -l help-all -d "Show help message including Qt specific options and quit"

complete -c ktimedwallpaperbuilder -l quality -d "Specify the quality of the encoded images (from 0 to 100)" -r
complete -c ktimedwallpaperbuilder -l output -d "Specify the file where the output will be written" -r
complete -c ktimedwallpaperbuilder -l discard-color-profile -d "Discard embedded color profile"
complete -c ktimedwallpaperbuilder -l lossless -d "Use lossless coding"
